from tkinter import Tk, Label, Listbox, END, Button, StringVar
from datetime import datetime

root = Tk()
is_connected = False

def show_main_window():
    label = Label(root, text="Bluetooth connector")
    label.pack()
    button = Button(root, text="Połącz", command=connect_with_device)
    button.pack()


def connect_with_device():
    is_connected = True


def show_devices():
    button = Button(root, text="Wyszukaj urządzenia", command=show_avaible_devices)
    button.pack()


def show_avaible_devices():
    devices_tmp = ["DeviceOne", "Xperia 2137", "HTC Five"]
    listbox = Listbox()
    listbox.insert(END, *devices_tmp)
    listbox.pack()


def show_results():
    button = Button(root, text="Wyświetl wyniki", command=start_results_acquisition)
    button.pack()

def start_results_acquisition():
    result = StringVar()
    description_label = Label(root, text="Wartość")
    result_label = Label(root, textvariable=result)
    result.set(datetime.now())
    description_label.pack()
    result_label.pack()


if __name__ == "__main__":
    show_main_window()
    show_devices()
    # if is_connected:
    show_results()

    root.mainloop()